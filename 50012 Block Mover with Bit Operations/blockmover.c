#include <stdio.h>
#include "blockmover.h"
#include <assert.h>

// You can write your own functions here.

static const BLOCK left_border    = 0x0101010101010101;
static const BLOCK right_border   = 0x8080808080808080;
static const BLOCK top_border     = 0x00000000000000FF;
static const BLOCK bottom_border  = 0xFF00000000000000;
static const BLOCK print_mask     = 0x0000000000000001;

typedef enum which_border {
   Left = 0,
   Right,
   Top,
   Bottom
}which_border;

int hit_border( BLOCK* blk_ptr, which_border b ){
   BLOCK border = 0;
   switch(b){
      case(Left):   { border = left_border   ; break; }
      case(Right):  { border = right_border  ; break; }
      case(Top):    { border = top_border    ; break; }
      case(Bottom): { border = bottom_border ; break; }
      default:      assert( 0 && "wrong border, WTF");
   }

   return (int)( ((*blk_ptr)&border) != 0 );
}

void printBlock(BLOCK *block) {
   // your implementation here
   BLOCK tmp = *block;
   for( int i = 0; i < sizeof(BLOCK); ++i ){
      for( int j = 0; j < 8; ++j ){
         printf( "%d", (int)(tmp & print_mask) );
         tmp >>= 1;
      }
      printf( "\n" );
   }
}

void initialize(unsigned long long int *block, int row, int column, int size) {
   // your implementation here
   BLOCK ret = 0;
   BLOCK tmp = (((BLOCK)1) << size) - 1;
   tmp <<= column;
   tmp = tmp & top_border;

   for( int i = 0; i < 8 && i < row; ++i ){
      tmp <<= 8;
   }
   for( int i = 0; i < size && (i+row) < 8; ++i, tmp <<= 8 ){
      ret |= tmp;
   }

   *block = ret;
}

int moveLeft(unsigned long long int *block) {
   // your implementation here
   if( hit_border( block, Left ) ) return 0;
   *block >>= 1;
   return 1;
}

int moveRight(unsigned long long int *block) {
   // your implementation here
   if( hit_border( block, Right ) ) return 0;
   *block <<= 1;
   return 1;
}

int moveUp(unsigned long long int *block) {
   // your implementation here
   if( hit_border( block, Top ) ) return 0;
   *block >>= 8;
   return 1;
}

int moveDown(unsigned long long int *block) {
   // your implementation here
   if( hit_border( block, Bottom ) ) return 0;
   *block <<= 8;
   return 1;
}
