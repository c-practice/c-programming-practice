#ifndef MY_BLOCKMOVER_H
#define MY_BLOCKMOVER_H

void printBlock(unsigned long long int *block);
void initialize(unsigned long long int *block, int row, int column, int size);
int moveLeft(unsigned long long int *block);
int moveRight(unsigned long long int *block);
int moveUp(unsigned long long int *block);
int moveDown(unsigned long long int *block);

typedef unsigned long long int BLOCK;

#endif // mY_BLOCKMOVER_H
