#include <time.h>
#include <stdlib.h>
#include <stdio.h>

int main(){
   srand(time(NULL));

   FILE* fp = fopen("You Not Respect Lake. Lake is Angry AF.txt", "wb");
   char  output_buf[1024] = {0};

   unsigned n = rand()%10 + 1, m = rand()%10 + 1;

   sprintf( output_buf, "%d %d\n", n, m );
   fputs( output_buf, fp );

   char* lake = calloc( n*m, 1 );
   for( unsigned i = 0; i < (n*m); ++i ){
      if(rand()%2) lake[i] = 1;
   }

   for( unsigned i = 0; i < n; ++i ){
      unsigned offset = 0;
      for( unsigned j = 0; j < m; ++j ){
         offset += sprintf( output_buf+offset, "%d", lake[i*m+j] );
         if( j < m-1 ){
            offset += sprintf( output_buf+offset, " " );
         }
      }
      sprintf( output_buf+offset, "\n" );
      fputs( output_buf, fp );
   }

   fclose( fp );

   return 0;
}
