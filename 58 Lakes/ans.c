#include <stdio.h>
#include <stdlib.h>
#include <string.h>

static const size_t stdin_buf_size = 1024;

static const char Ignore = 0;
static const char New    = 1;
static const char Done   = 'd';

size_t my_dfs( char** lake_map, size_t row, size_t col, size_t row_limit, size_t col_limit ){
   lake_map[row][col] = Done;
   size_t ret = 1;

   size_t prev_row = ( row             )? row-1 : row;
   size_t next_row = ((row+1)<row_limit)? row+1 : row;
   size_t prev_col = ( col             )? col-1 : col;
   size_t next_col = ((col+1)<col_limit)? col+1 : col;

   if( lake_map[row     ][prev_col] == New )
      ret += my_dfs( lake_map, row,      prev_col, row_limit, col_limit );
   if( lake_map[row     ][next_col] == New )
      ret += my_dfs( lake_map, row,      next_col, row_limit, col_limit );
   if( lake_map[prev_row][col     ] == New )
      ret += my_dfs( lake_map, prev_row, col,      row_limit, col_limit );
   if( lake_map[next_row][col     ] == New )
      ret += my_dfs( lake_map, next_row, col,      row_limit, col_limit );

   return ret;
}

int qsort_compare( const void* a, const void* b ){
   const unsigned* p1 = (const unsigned*)a;
   const unsigned* p2 = (const unsigned*)b;

   if( (*p1) > (*p2) ) return -1;
   else if ( (*p1) < (*p2) ) return 1;
   else return 0;
}


int main(){
   char* const stdin_buf = malloc(stdin_buf_size);
   char* tmp_ptr = stdin_buf;
   fgets( stdin_buf, stdin_buf_size, stdin );

   size_t N = strtol( stdin_buf, &tmp_ptr, 10);
   size_t M = strtol( tmp_ptr, NULL, 10 );
   size_t total_lake_count  = 0;
   size_t maximum_lake_size = 0;

   unsigned* lake_sizes = calloc( sizeof(*lake_sizes) * (N*M/2) + 1, sizeof(unsigned) );

   // Initialize map from "stdin"
   char* lake_map_bare = malloc(N * M);
   char** lake_map = malloc( sizeof(*lake_map) * N );
   for( size_t i = 0; i < N; ++i ){
      memset( stdin_buf, 0, stdin_buf_size );
      fgets( stdin_buf, stdin_buf_size, stdin );
      tmp_ptr = stdin_buf;
      lake_map[i] = lake_map_bare + M*i;
      for( size_t j = 0; j < M; ++j ){
         lake_map[i][j] = strtol( tmp_ptr, &tmp_ptr, 10 );
      }
   }

   // DFS count lake
   // (char)0: land, don't traverse
   // (char)1: lake, un-discovered;
   // 'd': discovered;
   // After which we shall have "lake_map" being composed of ((char)0) and 'd'.
   for( size_t i = 0; i < N; ++i ){
      for( size_t j = 0; j < M; ++j ){
         if( lake_map[i][j] == New ){
            lake_sizes[total_lake_count] = my_dfs(lake_map, i, j, N, M);
            ++ total_lake_count;
         }
      }
   }

   qsort( lake_sizes, total_lake_count, sizeof(unsigned), qsort_compare );

   for( size_t i = 0; i < total_lake_count; ++i ){
      printf( "%u\n", lake_sizes[i] );
   }

   free( stdin_buf );
   free( lake_map );
   free( lake_map_bare );
   free( lake_sizes );
}
