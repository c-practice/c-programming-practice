#include <stdio.h>
#include <stdlib.h>
#include <math.h>
 
typedef struct dimensions {
   unsigned long a;
   unsigned long b;
   unsigned long c;
   unsigned long d;
   unsigned long e;
   unsigned long area;
   unsigned long volume;
 
   void (*init) (struct dimensions*, unsigned long*);
   void (*calc) (struct dimensions*);
   unsigned long (*get_volume) (struct dimensions*);
   unsigned long (*get_area) (struct dimensions*);
} dimensions;
 
void init_dimensions( dimensions* D, unsigned long* data ){
   if( data == NULL ){
      D->a = D->b = D->c = D->d = D->e = 0;
   }else{
      D->a = data[0];
      D->b = data[1];
      D->c = data[2];
      D->d = data[3];
      D->e = data[4];
   }
   D->area = 0;
   D->volume = 0;
}
 
void calc_dimensions( dimensions* D ){
   unsigned long volume = ((D->a)*(D->b)*(D->c));
   volume -= (D->d)*((D->a)-2*(D->e))*((D->b)-2*(D->e))*2;
   volume -= (D->d)*((D->b)-2*(D->e))*((D->c)-2*(D->e))*2;
   volume -= (D->d)*((D->a)-2*(D->e))*((D->c)-2*(D->e))*2;
   D->volume = volume;
 
   unsigned long area = ((D->a)*(D->b) + (D->b)*(D->c) + (D->c)*(D->a))*2;
   area += ((D->d)*((D->a)-(D->e)*2))*8;
   area += ((D->d)*((D->b)-(D->e)*2))*8;
   area += ((D->d)*((D->c)-(D->e)*2))*8;
   D->area = area;
}
 
unsigned long get_volume(dimensions* D){
   return D->volume;
}
 
unsigned long get_area(dimensions* D){
   return D->area;
}
 
void dimensions_tie_ptr( dimensions* D,
   void (*init) (struct dimensions*, unsigned long*),
   void (*calc) (struct dimensions*),
   unsigned long (*get_area) (struct dimensions*),
   unsigned long (*get_volume) (struct dimensions*)
){
   D->init = init;
   D->calc = calc;
   D->get_area = get_area;
   D->get_volume = get_volume;
}
 
int main(){
   char buf [32] = {0};
   unsigned long dim [5] = {0};
   char* ptr = buf;
   dimensions D;
 
   for( unsigned long i = 0; i < 5; ++i ){
      fgets( ptr, 32, stdin );
      dim[i] = strtol( ptr, NULL, 10 );
   }
   dimensions_tie_ptr( &D, init_dimensions, calc_dimensions, get_area, get_volume );
   D.init( &D, dim );
   D.calc( &D );
   printf( "%ld\n%ld\n", D.get_area(&D), D.get_volume(&D) );
 
   return 0;
}
