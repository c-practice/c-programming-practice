#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

static const int input_size_limit = 2100;  // 64-bit shall not be more than 20 chars

void my_trim_trailing_space( char* buf, int size ){
   char* ptr = buf;
   while( ptr-buf < size && *ptr != '\n' && *ptr != '\r' ){
      ++ ptr;
   }
   --ptr;
   while( ptr-buf >= 0 && isspace(*ptr) ){
      --ptr;
   }
   ++ptr;
   *ptr = '\n';
   ++ptr;
   memset( ptr, '\0', size-(ptr-buf) );
}

int main(){
   // initilize
   char *input_buf = malloc(input_size_limit * sizeof(char));
   fgets( input_buf, input_size_limit, stdin );
   my_trim_trailing_space( input_buf, input_size_limit );
   char* tmp_ptr;
   int input[100] = {0};
   int output[100] = {0};
   int real_input_size = 0;
   int two_palindrome_idx_0 = 0;
   int two_palindrome_idx_1 = 0;
   int two_palindrome_len = 0;
   input[0] = (int) strtol( input_buf, &tmp_ptr, 10 );
   ++real_input_size;
   while( *tmp_ptr != '\0' && *tmp_ptr != '\n' && *tmp_ptr != '\r' ){
      char* tmp_ptr_bak = tmp_ptr;
      input[real_input_size++] = (int) strtol( tmp_ptr_bak, &tmp_ptr, 10 );
      if( tmp_ptr == tmp_ptr_bak ){
         break;
      }
   }
   if( real_input_size == 1 ){
      printf( "%d\n", input[0] );
      free( input_buf );
      return 0;
   }
   int *aux_arr = malloc((real_input_size*2-1) * sizeof(int));

   // check for all palindromes; O(n^2), unfortunately
   //
   // suppose input is "Dio_Sama!" of which size is 11
   // notice palindrome could be even or odd in size
   // hence if we were to find all of them, we need (size*2-1)==21 array
   // to record "D i o _ S a m a !"
   //           "0 1 2 3 4 5 6 7 8"
   //           "0123456789ABCDEFG"
   //
   // deal with even indices of `aux_arr`: palindrome is odd in size
   for( int i = 0; i < real_input_size; ++i ){
      int tmp_len = 1;
      for( int j = 1; (i-j) >= 0 && (i+j) < real_input_size; ++j ){
         if( input[i-j] == input[i+j] ) ++tmp_len;
         else break;
      }
      aux_arr[i*2] = tmp_len;
   }
   // deal with odd indices of `aux_arr`: palindrome is even in size
   for( int i = 0; i < real_input_size-1; ++i ){
      int tmp_len = 0;
      for( int j = 1; (i-j+1) >= 0 && (i+j) < real_input_size; ++j ){
         if( input[i-j+1] == input[i+j] ) ++tmp_len;
         else break;
      }
      aux_arr[i*2+1] = tmp_len;
   }

   // scan through lengths of all palindromes
   // check if dual palindrome
   // record length and index of dual palindrome
   // O(n^2)
   for( int i = 0; i < real_input_size*2-1; ++i ){
      if( i%2 ){
         if (aux_arr[i] == 0)
            continue;
         for (int j = i+1; j < real_input_size*2-1; ++j) {
            if ( i/2 + aux_arr[i] >= j/2 - aux_arr[j] ) {
               int tmp_len = 2*aux_arr[j] - (!(j%2));
               tmp_len += (j/2 - aux_arr[j] - i/2)*2;
               if ( tmp_len >= two_palindrome_len ) {
                  two_palindrome_idx_0 = i;
                  two_palindrome_idx_1 = j;
                  two_palindrome_len = tmp_len;
               }
            }
         }
      }else{ // !( i%2 )
         for (int j = i+1; j < real_input_size*2-1; ++j) {
            if ( i/2 + aux_arr[i] > j/2 - aux_arr[j] ) {
               int tmp_len = 2*aux_arr[j] - (!(j%2));
               tmp_len += (j/2 - aux_arr[j] - i/2)*2 + 1;
               if ( tmp_len >= two_palindrome_len) {
                  two_palindrome_idx_0 = i;
                  two_palindrome_idx_1 = j;
                  two_palindrome_len = tmp_len;
               }
            }
         }
      } // if (i%2) {} else {}
   }

   // Want last longest dual palindrome
   // Its first component and second component may or may not overlap
   // If overlap, the longer the second component the better
   //
   // Second Palindrome:
   int palindrome_1_len = 2*aux_arr[two_palindrome_idx_1] - (!(two_palindrome_idx_1%2));
   int palindrome_1_start = two_palindrome_idx_1/2 + 1 - aux_arr[two_palindrome_idx_1];
   memcpy(output+two_palindrome_len-palindrome_1_len,
      input+palindrome_1_start,
      palindrome_1_len*sizeof(int) );

   // First Palindrome:
   int palindrome_0_len = two_palindrome_len - palindrome_1_len;
   int palindrome_0_start = palindrome_1_start - palindrome_0_len;
   memcpy(output, input+palindrome_0_start, palindrome_0_len*sizeof(int) );

   // reuse "input_buf" and "tmp_ptr" as output buffer
   tmp_ptr = input_buf;
   for (int i = 0; i < two_palindrome_len; ++i) {
      int sprintf_errno = sprintf( tmp_ptr, "%d ", output[i] );
      if( sprintf_errno >= 0 ){
         tmp_ptr += sprintf_errno;
      }
   }
   tmp_ptr--;
   *tmp_ptr = '\0';
   printf( "%s\n", input_buf );

   free(input_buf);
   free(aux_arr);

   return 0;
}
