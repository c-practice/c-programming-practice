#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>

static const size_t MY_MAX_INPUT_SIZE = (1 << 20) + 1;
static const size_t MY_MAX_STDIN_SIZE = 512;

char* my_fgets(char* buf,
   size_t size,
   FILE* fp,
   int* have_change_line,
   size_t* real_size
){
   size_t s = 0;
   int met_change_line = 0;
   char c;
   while( s < size ){
      c = fgetc(fp);
      buf[s] = c;
      if( c == EOF ) break;
      ++s;
      if( c == '\n' ){
         met_change_line = 1;
         break;
      }
   }
   *have_change_line = met_change_line;

   if( s == 0 && c == EOF ) return NULL;
   buf[s] = 0;
   *real_size = s;
   return buf;
}


int main(){
   char   *buf = malloc(MY_MAX_INPUT_SIZE);
   char   stdin_buf[MY_MAX_STDIN_SIZE];
   memset( stdin_buf, 0, MY_MAX_STDIN_SIZE );
   size_t file_count = 0;
   char* tmp_ptr;
   if (buf == NULL) {
      perror("Insufficient Memory\n");
      return 1;
   }
   fgets( stdin_buf, MY_MAX_STDIN_SIZE, stdin );
   tmp_ptr = strtok( stdin_buf, " ");
   tmp_ptr = strtok( NULL, " ");
   file_count = strtol(tmp_ptr, NULL, 10);

   size_t current_line_words = 0;
   size_t current_line_len   = 0;
   int    last_instance_done = 1;
   int    met_newline = 0;
   int    last_line_end_with_space = 0;

   for( size_t f = 0; f < file_count; ++f ){
      // many file systems, includeing ext4, allows a maximum of 255 bytes of filename
      char pfilename [256];
      sprintf(pfilename, "%s-%03zu", stdin_buf, f);
      FILE* fp = fopen( pfilename, "rb" );
      if( fp == NULL ){
         perror("Wrong Input Filename\n");
         return 1;
      }
      while ((my_fgets(buf, MY_MAX_INPUT_SIZE, fp, &met_newline,
               &current_line_len) != NULL)) {
         int current_line_end_with_space = isspace(buf[current_line_len - 1]);
         char *token = strtok(buf, " \n\r");
         if (last_instance_done) {
            current_line_words = 1;
         } else {
            if (last_line_end_with_space || token != buf) {
               ++current_line_words;
            }
         }
         if (__builtin_expect((token == NULL), 0)) {
            printf("0\r\n");
            continue;
         }
         while ((token = strtok(NULL, " \n\r")) != NULL) {
            ++current_line_words;
         }
         if (met_newline) {
            printf("%zu\r\n", current_line_words);
         }
         last_instance_done = met_newline;
         last_line_end_with_space = current_line_end_with_space;
         if (feof(fp) || ferror(fp))
            break;
      }
   }
}
