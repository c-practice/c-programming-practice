#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <assert.h>

size_t my_trim_trailing_space( char* buf, size_t size ){
   size_t ret = 0; // spaces eliminated
   char* ptr = buf;
   while( ptr-buf < size && *ptr != '\n' && *ptr != '\r' ){
      ++ ptr;
   }
   --ptr;
   while( ptr-buf >= 0 && isspace(*ptr) ){
      ++ret;
      --ptr;
   }
   ++ptr;
   *ptr = '\n';
   ++ptr;
   memset( ptr, '\0', size-(ptr-buf) );
   return ret;
}

typedef struct char_vector {
   size_t arr_capacity;
   size_t arr_length;
   char*  arr_ptr;

   size_t (*length)    (struct char_vector*);
   void   (*push_back) (struct char_vector*, char);
   char   (*pop)       (struct char_vector*);
   char   (*tail)      (struct char_vector*);
   char   (*at)        (struct char_vector*, size_t);
   void   (*resize)    (struct char_vector*, size_t);
   void   (*destruct)  (struct char_vector*);
   void   (*trim_tail) (struct char_vector*);

}char_vector;

size_t char_vector_arr_length(char_vector *cvp) {
  return cvp->arr_length;
}

void char_vector_push_back( char_vector* cvp, char c ){
   cvp->resize( cvp, cvp->arr_length+1 );
   (cvp->arr_ptr)[cvp->arr_length-1] = c;
}

char char_vector_pop( char_vector* cvp ){
   if( cvp->arr_length ){
      char ret = cvp->arr_ptr[cvp->arr_length-1];
      cvp->resize(cvp, cvp->arr_length-1);
      return ret;
   }else {
      fprintf( stderr, "pop when empty" );
      return 0;
   }
}

char char_vector_tail( char_vector* cvp ){
   if( cvp->arr_length ){
      return (cvp->at(cvp, cvp->arr_length-1));
   }else{
      fprintf( stderr, "access tail when empty" );
      return 0;
   }
}

char char_vector_at( char_vector* cvp, size_t s){
   if( s < cvp->arr_length ){
      return cvp->arr_ptr[s];
   }else{
      assert( 0 && "Out of Range Access" );
   }
}

void char_vector_resize( char_vector* cvp, size_t s ){
   if( cvp->arr_capacity == 0 ){
      cvp->arr_ptr = malloc(1);
      cvp->arr_capacity = 1;
   }
   if( s >= cvp->arr_capacity ){
      while( cvp->arr_capacity <= s ) cvp->arr_capacity *= 2;
      char* arr_ptr_bak = cvp->arr_ptr;
      cvp->arr_ptr = realloc( cvp->arr_ptr, cvp->arr_capacity );
      if( cvp->arr_ptr == NULL ){
         cvp->arr_ptr = arr_ptr_bak;
         assert( 0 && "memory insufficient" );
      }
   }
   cvp->arr_length = s;
}

void char_vector_destruct( char_vector* cvp ){
   cvp -> arr_capacity = 0;
   cvp -> arr_length = 0;
   free( cvp->arr_ptr );
   cvp -> arr_ptr = NULL;
}

void char_vector_trim_tail( char_vector* cvp ){
   size_t spaces = my_trim_trailing_space(
      cvp->arr_ptr, cvp->length(cvp)
   );
   cvp->resize( cvp, cvp->length(cvp)-spaces );
}

char_vector create_char_vector(){
   char_vector ret;
   ret.length    = char_vector_arr_length;
   ret.push_back = char_vector_push_back;
   ret.pop       = char_vector_pop;
   ret.tail      = char_vector_tail;
   ret.at        = char_vector_at;
   ret.resize    = char_vector_resize;
   ret.destruct  = char_vector_destruct;
   ret.arr_capacity = 0;
   ret.resize(&ret, 0);
   return ret;
}

void create_char_vector_ptr(char_vector** ptr){
   free(*ptr);
   *ptr = malloc(sizeof(char_vector));
   **ptr = create_char_vector();
}

int main(){

   static const double pi = 3.1415926;

   char_vector* input_vector = NULL;
   create_char_vector_ptr(&input_vector);
   char tmp;
   while( (tmp = fgetc(stdin)) != EOF ){
      input_vector->push_back(input_vector, tmp);
      if( tmp == '\n' || tmp == '\r' ){
         break;
      }
   }

   char* input_buf_bak = malloc(
      input_vector -> length(input_vector)
   );
   memcpy(input_buf_bak, input_vector->arr_ptr,
      input_vector->length(input_vector) );
   input_vector -> destruct (input_vector);
   double abc[3];

   char* tmp_ptr = input_buf_bak;
   for( int i = 0; i < 3; ++i ){
      char* tmp_ptr_bak = tmp_ptr;
      abc[i] = strtod( tmp_ptr_bak, &tmp_ptr );
   }

   double area = abc[2]*abc[2]*0.75;
   if( abc[2] > abc[0] ){
      area += (abc[2]-abc[0])*(abc[2]-abc[0])*0.25;
   }
   if( abc[2] > abc[1] ){
      area += (abc[2]-abc[1])*(abc[2]-abc[1])*0.25;
   }
   area *= pi;

#ifdef DEBUG
   printf( "%f %f %f\n", abc[0], abc[1], abc[2] );
#endif // DEBUG

   printf( "%f\n", area );

   free( input_buf_bak );
   free( input_vector );
}
